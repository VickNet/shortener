const CreateError = require("http-errors")
const express = require('express')

const router = express.Router()

const Link = require('../models/links')
const passport = require('passport')
const jwt = require('jsonwebtoken')
const config = require('../config/database')

router

.post('/add', passport.authenticate('jwt', {session: false}), (req, res, next) => {
    let newLink = new Link({
        URL: req.body.link,
        author: req.user.username
    })

    Link.addLink(newLink, (err, link) => {
        if (err)
            res.status(400).send("What the f*ck you post, bro?")
        res.status(200).send(link)
    })
})

.post('/anonim', (req, res, next) => {
    let newLink = new Link({
        URL: req.body.link
    })
    Link.addLink(newLink, (err, link) => {
        if (err)
            res.status(400).send("What the f*ck you post, bro?")
        res.status(200).send(link)
    })
})

.get('/my', passport.authenticate('jwt', {session: false}), (req, res, next) => {
    Link.getLinks(req.user.username, (err, links) => {
        if (err)
            res.status(400).send("What the f*ck you get, bro?")
        else
            res.send(links)
    })
})

module.exports = router