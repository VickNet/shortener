import { Router } from '@angular/router';
import { MdSnackBar } from '@angular/material';
import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';

import { ApiService } from "../../services/api.service"

const PASSWORD_REGEX = /^.{6,36}$/;
const USERNAME_REGEX = /^[a-zA-Z]{4,20}$/;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  username: any;
  password: any;

  constructor(
    private api: ApiService,
    private router: Router,
    private snackBar: MdSnackBar
  ) {}

  ngOnInit() {
  }

  toast(message: string) {
    this.snackBar.open(message, "Close", {
      duration: 3000
    });
  }

  passwordFormControl = new FormControl('', [
    Validators.required,
    Validators.pattern(PASSWORD_REGEX)]);
  
  usernameFormControl = new FormControl('', [
    Validators.required,
    Validators.pattern(USERNAME_REGEX)
  ])

  onLogin() {
    const user = {
      username: this.username,
      password: this.password
    }
    this.api.login(user).subscribe(
    data => {
      if(data.ok) {
        let parse = JSON.parse(data["_body"]);
        localStorage.clear();
        this.api.saveToken(parse.token, parse.username)
        this.toast("Yo're signed now.")
        this.router.navigate(['/'])
      }
    },
    error => {
      this.toast(error._body);
    })
  }
}
