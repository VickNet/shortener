import { Router } from '@angular/router';
import { DataSource } from '@angular/cdk';
import { MdDialog, MdDialogRef } from '@angular/material';
import { Observable } from 'rxjs/Observable';
import { MdPaginator } from '@angular/material';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Component, ViewChild, OnInit } from '@angular/core';

import { ApiService } from '../../services/api.service';

import 'rxjs/add/operator/startWith';
import 'rxjs/add/observable/merge';
import 'rxjs/add/operator/map';

const URL_REGEX = /((([A-Za-z]{3,9}:(?:\/\/)?)(?:[\-;:&=\+\$,\w]+@)?[A-Za-z0-9\.\-]+|(?:www\.|[\-;:&=\+\$,\w]+@)[A-Za-z0-9\.\-]+)((?:\/[\+~%\/\.\w\-_]*)?\??(?:[\-\+=&;%@\.\w_]*)#?(?:[\.\!\/\\\w]*))?)/;

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  url: any;
  URLs = JSON.parse(localStorage.getItem('URLs')) || [];
  displayedColumns = ['original', 'created', 'short', 'clicks'];

  dataSource: Source | null;
  database = new Database(this.api);

  constructor(
    private router: Router,
    private api: ApiService,
    public dialog: MdDialog
  ) { }

  @ViewChild(MdPaginator) paginator: MdPaginator;

  shortURL() {

    const link = {link: `http://${this.url}`}
    if (this.api.loggedIn()) {
      this.api.shortURL(link).subscribe(data => {
        let dialogRef = this.dialog.open(ModalDialog);
        dialogRef.componentInstance.link = JSON.parse(data["_body"]);
        let database = new Database(this.api);
        let dataSource: Source | null;
        this.dataSource = new Source(database, this.paginator);
      })
    } else {
      this.api.anonim(link).subscribe(data => {
        if (data.ok) {
          let link = JSON.parse(data["_body"]);
          let dialogRef = this.dialog.open(ModalDialog);
          dialogRef.componentInstance.link = link;
          this.URLs.push(link);
          localStorage.setItem('URLs', JSON.stringify(this.URLs));
          let database = new Database(this.api);
          let dataSource: Source | null;
          this.dataSource = new Source(database, this.paginator);
        }
      })
    }
  }

  ngOnInit() {
    this.dataSource = new Source(this.database, this.paginator);
  }
}

@Component({
  selector: 'modal-dialog',
  template: `
    Your short URL
    <br>
    {{hostname}}/{{link.key}}
  `
})
export class ModalDialog {
  public link;
  public hostname = location.hostname;
}

export interface Link {
  id: string;
  URL: string;
  created: string;
  short: string;
  acceses: number;
}

export class Database {
  
  dataChange: BehaviorSubject<Link[]> = new BehaviorSubject<Link[]>([]);
  get data(): Link[] { return this.dataChange.value; }

  constructor(private api: ApiService) {

    if(api.loggedIn()) {
      api.getURLs().subscribe(URLs => {
        for(let url of URLs)
          { this.addLink(url); }
      })
    } else {
      let data = localStorage.getItem('URLs');
      if (data) {
        let URLs = JSON.parse(data);
        for(let url of URLs)
          { this.addLink(url) }
      }
    }
  }

  addLink(url) {
    const newLink = this.data.slice();
    newLink.unshift(this.createNewLink(url.URL, url.created, url.key, url.acceses));
    this.dataChange.next(newLink);
  }

  private createNewLink(url, created, key, clicks) {
    return {
      id: (this.data.length + 1).toString(),
      URL: url,
      created: created,
      short: key,
      acceses: clicks
    };
  }
}

export class Source extends DataSource<any> {
  constructor(private database: Database, private paginator: MdPaginator) {
    super();
  }

  connect(): Observable<Link[]> {
    const displayDataChanges = [
      this.database.dataChange,
      this.paginator.page,
    ];

    return Observable.merge(...displayDataChanges).map(() => {
      const data = this.database.data.slice();

      const startIndex = this.paginator.pageIndex * this.paginator.pageSize;
      return data.splice(startIndex, this.paginator.pageSize);
    });
  }

  disconnect() {}
}
  